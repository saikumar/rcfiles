;; Install emacs > 24.5 (brew install emacs)
;; Install pip (easy_install pip)
;; Install virtualenv (pip install virtualenv)
;; Install pylint (pip install pylint)

(require 'package) 
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(package-initialize) 

(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))
(require 'use-package)


(use-package magit
  :ensure t)

(use-package helm
  :ensure t
  :config
  (require 'helm)
  (require 'helm-config)
  (helm-mode 1))

;; Install pylint first
;; sudo pip install pylint
(use-package flycheck
	:ensure t
	:config
	(add-hook 'after-init-hook #'global-flycheck-mode))


(use-package sr-speedbar
   :ensure t)

(use-package anaconda-mode
	     :ensure t
	     :config
	     (add-hook 'python-mode-hook 'anaconda-mode)
	     (add-hook 'python-mode-hook 'eldoc-mode))

(use-package jedi
  :ensure t
  :config
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:complete-on-dot t))


(defun python-add-breakpoint ()
  "Add a break point"
  (interactive)
  (newline-and-indent)
  (insert "import ipdb; ipdb.set_trace()")
  (highlight-lines-matching-regexp "^[ ]*import ipdb; ipdb.set_trace()"))

(define-key python-mode-map (kbd "C-c C-b") 'python-add-breakpoint)
